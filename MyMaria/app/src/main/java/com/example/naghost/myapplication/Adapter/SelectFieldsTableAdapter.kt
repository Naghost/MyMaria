package com.example.naghost.myapplication.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.naghost.myapplication.Asynctask.ConnectionTask
import com.example.naghost.myapplication.Holder.ViewHolderTables
import com.example.naghost.myapplication.Holder.ViewHolderTablesFields
import com.example.naghost.myapplication.R
import com.example.naghost.myapplication.model.modelSelect
import com.example.naghost.myapplication.model.modelSelectTable


class SelectFieldsTableAdapter(var context: Context, items :ArrayList<modelSelectTable>) : BaseAdapter(){
    var items : ArrayList<modelSelectTable>? =null;
    var connectionTask: ConnectionTask?=null;
    var databaseName:String ="";
    init {
        this.items=items

    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var holder: ViewHolderTablesFields?;
        var vista: View? = convertView;

        if (vista == null){
            vista = LayoutInflater.from(context).inflate(R.layout.listviewfieldstable, null)
            holder = ViewHolderTablesFields(vista)
            vista.tag = holder
        }else{
            holder = vista.tag as? ViewHolderTablesFields
        }

        val item = getItem(position) as modelSelectTable

        holder!!.nombre!!.text =item.name
        holder!!.type!!.text=item.type
        holder!!.key!!.text=item.key
        holder!!.nulleable!!.text=item.nullity
        holder.item =item
        holder.checkBox!!.setOnClickListener(holder)
        holder.context=context
        holder.nombre!!.setOnClickListener(holder)
        holder.checkBox!!.setChecked(item.active!!)
        holder.connectionTask=connectionTask
        holder.databaseName=databaseName

        return vista!!
    }

    override fun getItem(position: Int): Any {
        return items?.get(position)!!
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return items?.count()!!
    }

}