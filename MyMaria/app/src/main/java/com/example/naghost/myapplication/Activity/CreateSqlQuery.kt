package com.example.naghost.myapplication.Activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.example.naghost.myapplication.Asynctask.ConnectionTask
import com.example.naghost.myapplication.R

class CreateSqlQuery : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        var radioButton:RadioButton = findViewById(R.id.radioButton)
        if (radioButton.isChecked()){
            var intent: Intent = Intent(this, OperationResults::class.java)

            var bundle:Bundle = Bundle()
            connectionTask!!.query=sqlQuery!!.text.toString()
            bundle.putParcelable("object", connectionTask)
            intent.putExtra("bundle",bundle)
            startActivity(intent)
            finish()
        }else{
            connectionTask!!.operation=10
            connectionTask!!.query=sqlQuery!!.text.toString()
            connectionTask!!.start()

            while(connectionTask!!.isAlive && !connectionTask!!.isInterrupted){
                Thread.sleep(1000)
            }
            if (connectionTask!!.error){
                Toast.makeText(this,connectionTask!!.msgError, Toast.LENGTH_LONG).show()
            }else{
                Toast.makeText(this,"Operation are made succesfully", Toast.LENGTH_LONG).show()
            }
            finish()
        }
    }

    var connectionTask:ConnectionTask?=null

    var sqlQuery:EditText?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_sql_query)

        var bundle:Bundle = getIntent().getExtras()
        var auxbundle:Bundle = bundle.getBundle("bundle")
        connectionTask = auxbundle.getParcelable("object")

        sqlQuery=findViewById(R.id.EditTextSQLQuery)
        var button: Button = findViewById(R.id.ButtonExecute)
        button.setOnClickListener(this)
    }
}
