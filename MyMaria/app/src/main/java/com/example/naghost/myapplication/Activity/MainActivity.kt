package com.example.naghost.myapplication.Activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.naghost.myapplication.R


class MainActivity :AppCompatActivity(), View.OnClickListener{
    var server: EditText?=null;
    var port: EditText?=null
    var user: EditText? =null
    var password: EditText? = null
    var connectButton: Button?=null

    override fun onClick(v: View?) {

        if(!server!!.text.isNullOrBlank()) {
            var result: String = server?.text.toString()

            val intent = Intent(this, SelectDataBaseActivity::class.java)
            intent.putExtra("server", server?.text.toString())
            intent.putExtra("port", port?.text.toString())
            intent.putExtra("user", user?.text.toString())

            intent.putExtra("password", password?.text.toString())

            startActivity(intent)
            finish();
        }else{
            Toast.makeText(this
            , "Please check your data connection",Toast.LENGTH_LONG).show()
        }
    }

    private val TAG = "PermissionDemo"
    private val RECORD_REQUEST_CODE = 101
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

            val permission = ContextCompat.checkSelfPermission(this,
                    Manifest.permission.INTERNET)

            if (permission != PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "Permission to record denied")
                makeRequest()
            }

           server = findViewById(R.id.serverEditText) as EditText;
           port = findViewById(R.id.portEditText) as EditText;
           user = findViewById(R.id.userEditText) as EditText
           password = findViewById(R.id.passwordEditText) as EditText

           connectButton = findViewById(R.id.connectButton) as Button
           this.connectButton?.setOnClickListener(this);



        }




    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.INTERNET),
                RECORD_REQUEST_CODE)
    }
    }

