package com.example.naghost.myapplication.Adapter

import android.app.Activity
import android.content.Context
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import com.example.naghost.myapplication.Asynctask.ConnectionTask
import com.example.naghost.myapplication.Holder.ViewHolder
import com.example.naghost.myapplication.Holder.ViewHolderCreateTable
import com.example.naghost.myapplication.Holder.ViewHolderTablesFields
import com.example.naghost.myapplication.R

import com.example.naghost.myapplication.model.modelCreateTable


class CreateTableAdapter(var context: Context, items :ArrayList<modelCreateTable>) : BaseAdapter()
{
    var items : ArrayList<modelCreateTable>? =null;
    var connectionTask: ConnectionTask?=null;
    var type:ArrayAdapter<String>? = null;
    var predetermined:ArrayAdapter<String>?=null;
    var index:ArrayAdapter<String>? = null

    init {
        this.items=items

    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var holder: ViewHolderCreateTable? =null;
        var vista: View? = convertView;

        if (vista == null){
            vista = LayoutInflater.from(context).inflate(R.layout.listview_create_table, null)
            holder = ViewHolderCreateTable(vista)
            vista.tag = holder

        }
            holder = vista!!.tag as? ViewHolderCreateTable
        val item = getItem(position) as modelCreateTable
        holder?.item =item
        holder?.context=context
        holder?.connectionTask=connectionTask
        holder?.name!!.setText(item.name)
        holder?.type?.adapter=type
        holder?.type?.setSelection(item.typeInt)
        holder?.predetermined?.adapter=predetermined
        holder?.predetermined?.setSelection(item.predetermitadInt)
        holder?.index?.adapter=index
        holder?.index?.setSelection(item.indexInt)
        holder?.length?.setText(item.length)
        holder?.nullable!!.setChecked(item.nullable)
        holder?.aI!!.setChecked(item.aI)










        return vista!!
    }

    override fun getItem(position: Int): Any {
        return items?.get(position)!!
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return items?.count()!!
    }

}