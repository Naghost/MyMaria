package com.example.naghost.myapplication.Holder

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.*
import com.example.naghost.myapplication.Activity.DescribeTable
import com.example.naghost.myapplication.Asynctask.ConnectionTask
import com.example.naghost.myapplication.R
import com.example.naghost.myapplication.model.modelCreateTable
import com.example.naghost.myapplication.model.modelSelectTable

class ViewHolderCreateTable(vista: View): Activity(), View.OnClickListener, TextWatcher{


    override fun afterTextChanged(p0: Editable?) {

    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        if (name!!.text.hashCode()==p0!!.hashCode()){
            item!!.name=p0!!.toString()

        }
        if (length!!.text.hashCode()==p0!!.hashCode()){
            item!!.length=p0!!.toString()
        }
    }


    override fun onClick(v: View?) {
        when(v!!.tag){
            "null" -> item!!.nullable = nullable!!.isChecked

            "AI" -> item!!.aI = aI!!.isChecked


        }
    }

    var context: Context?=null;
    var connectionTask: ConnectionTask?=null;
    var name:EditText? = null
    var type:Spinner? = null
    var predetermined:Spinner?=null
    var length:EditText? = null
    var index:Spinner? = null
    var aI:CheckBox? = null
    var nullable:CheckBox? = null
    var item: modelCreateTable? =null
    init {
        name = vista.findViewById(R.id.EditTextCreateTableName) as EditText
        type = vista.findViewById(R.id.SpinnerTypeCreateTable)
        predetermined = vista.findViewById(R.id.SpinnerCreateTablePredetermined)
        index = vista.findViewById(R.id.SpinnerCreateTableIndex)
        length = vista.findViewById(R.id.EditTextCreateTableLength) as EditText
        aI = vista.findViewById(R.id.CheckBoxCreateTableAI)
        aI!!.setOnClickListener(this)
        nullable = vista.findViewById(R.id.CheckBoxCreateTableNull)
        nullable!!.setOnClickListener(this)
        name!!.addTextChangedListener(this)
        length!!.addTextChangedListener(this)


        predetermined!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                item!!.predeterminated=predetermined!!.selectedItem.toString()
                item!!.predetermitadInt=predetermined!!.selectedItemPosition
                item!!.index=index!!.selectedItem.toString()
                item!!.indexInt=index!!.selectedItemPosition
                item!!.type=type!!.selectedItem.toString()
                item!!.typeInt=type!!.selectedItemPosition
            }

        }
        index!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                item!!.predeterminated=predetermined!!.selectedItem.toString()
                item!!.predetermitadInt=predetermined!!.selectedItemPosition
                item!!.index=index!!.selectedItem.toString()
                item!!.indexInt=index!!.selectedItemPosition
                item!!.type=type!!.selectedItem.toString()
                item!!.typeInt=type!!.selectedItemPosition
            }

        }
        type!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                item!!.predeterminated=predetermined!!.selectedItem.toString()
                item!!.predetermitadInt=predetermined!!.selectedItemPosition
                item!!.index=index!!.selectedItem.toString()
                item!!.indexInt=index!!.selectedItemPosition
                item!!.type=type!!.selectedItem.toString()
                item!!.typeInt=type!!.selectedItemPosition
            }

        }
    }





}

