package com.example.naghost.myapplication.Asynctask

import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import com.example.naghost.myapplication.model.modelCreateTable
import com.example.naghost.myapplication.model.modelInsertRecord

import com.example.naghost.myapplication.model.modelSelect
import com.example.naghost.myapplication.model.modelSelectTable
import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException
import java.sql.*
import java.util.*
import kotlin.collections.ArrayList


class ConnectionTask() : Thread(), Parcelable {
    var listParameterConnection: ArrayList<String>? = null;
    var listOfResult: ArrayList<modelSelect>? = null;
    var listOfResultTables: ArrayList<modelSelectTable>? = null;
    var listOfParameters: ArrayList<modelCreateTable>? = null
    var listOfResultsConverted: ArrayList<modelInsertRecord>?=null
    var modelSingleton: modelCreateTable ?=null
    var connected: Boolean = false;
    var operation: Int = 0;
    var stmt: Statement? = null;
    var conn: Connection? = null;
    var dataBaseName: String = "";
    var tableName: String = "";
    var collate: String = "";
    var server: String = "";
    var port: String = "";
    var usr: String = "";
    var psw: String = "";
    var error: Boolean = false
    var msgError: String  =""
    var tableAfterName :String =""
    var query:String=""
    var rs:ResultSet?=null
    constructor(parcel: Parcel) : this() {
        connected = parcel.readByte() != 0.toByte()
        operation = parcel.readInt()
        dataBaseName = parcel.readString()
        tableName = parcel.readString()
        collate = parcel.readString()
        server = parcel.readString()
        port = parcel.readString()
        usr = parcel.readString()
        psw = parcel.readString()

        query =parcel.readString()
    }


    fun arrayListToParameters() {
        usr = listParameterConnection!!.get(2)
        psw = listParameterConnection!!.get(3)
        if (listParameterConnection?.get(1).equals("")) {
            port = "3306"
        } else {
            port = listParameterConnection?.get(1)!!
        }
        server = listParameterConnection!!.get(0)
    }

    @Synchronized
    override fun run() {
        super.run()
        val properties = Properties()
        with(properties) {
            put("user", usr)
            put("password", psw)
        }
        try {
            DriverManager.setLoginTimeout(1);
            Log.i("DriverManager:", DriverManager.getLoginTimeout().toString())
            conn = DriverManager.getConnection("jdbc:mysql://" + server + ":" + port, properties);

        } catch (a: CommunicationsException) {

        }
        if (conn != null) {
            if (!conn!!.isClosed) {
                connected = true
                when (operation) {
                    1  -> showDatabasesQuery()
                    2  -> createDatabase()
                    3  -> deleteSelectedDatabases()
                    4  -> showTables()
                    5  -> showFields()
                    6  -> createTable()
                    7  -> addingColumn()
                    8  -> deleteTable()
                    9  -> deleteColumn()
                    10 -> customOperationNotRs()
                    11 -> customOperationRs()
                    12 -> insertRecord()
                }

            }
        }
    }

    private fun insertRecord() {
        stmt = conn!!.createStatement()
        stmt?.executeUpdate("USE " + dataBaseName)
        stmt = conn!!.createStatement()
        var query:String ="INSERT INTO "+tableName+" ("
        var enter:Boolean = false
        for (result in listOfResultsConverted!!){
            if (!enter){
                if (!result.value.isNullOrEmpty()) {
                    query = query + result.name
                    enter = true
                }
            }else{
                if (!result.value.isNullOrEmpty()) {
                    query = query + ", " + result.name
                }
            }

        }
        query = query+") VALUES("

        enter = false
        for (result in listOfResultsConverted!!){
            if (!enter){
                if (!result.value.isNullOrEmpty()) {
                    query = query + result.value
                    enter=true
                }
            }else{
                if (!result.value.isNullOrEmpty()) {
                    query = query + ", " + result.value
                }
            }
        }
        query = query+")"

        try {
            stmt?.executeUpdate(query)
        }catch (a:SQLException){
            error=true
            msgError=a.localizedMessage
            Log.i("melin ", query)
        }finally {
            stmt!!.close()
            conn!!.close()
        }

    }

    private fun customOperationRs() {
        stmt = conn!!.createStatement()
        stmt?.executeUpdate("USE " + dataBaseName)
        stmt = conn!!.createStatement()
        try {
        rs = stmt?.executeQuery(query)
        }catch (e: SQLException){
            error=true
            msgError=e.localizedMessage

        }

    }

    private fun customOperationNotRs() {
        stmt = conn!!.createStatement()
        stmt?.executeUpdate("USE " + dataBaseName)
        stmt = conn!!.createStatement()
        try {
            stmt?.executeUpdate(query)
        }catch (e: SQLException){
            error=true
            msgError=e.localizedMessage
        }
    }

    private fun showDatabasesQuery() {
        stmt = conn!!.createStatement()
        val rs: ResultSet = stmt?.executeQuery("SHOW DATABASES")!!

        while (rs.next()) {
            var modelSelectDataBase = modelSelect()
            modelSelectDataBase.name = rs.getString(1)
            listOfResult?.add(modelSelectDataBase)
        }
        rs.close()
        conn!!.close()
        stmt!!.close()
    }


    fun createDatabase() {
        stmt = conn!!.createStatement()
        Log.i("param1", dataBaseName)
        Log.i("param2", collate)
        try {
            stmt?.executeUpdate("CREATE DATABASE " + dataBaseName + " CHARACTER SET utf8 COLLATE " + collate)!!
        } catch (a: SQLException) {
            error = true
            msgError=a.localizedMessage
        } finally {
            stmt?.close()
            conn?.close()
        }

    }

    private fun deleteSelectedDatabases() {

        for (i in 0 until listOfResult!!.size ) {

            if (listOfResult!!.get(i).active!!) {
                stmt = conn!!.createStatement()
                try {
                    stmt?.executeUpdate("DROP DATABASE " + listOfResult!!.get(i).name)
                }catch (a:SQLException){
                    error=true
                    msgError=a.localizedMessage
                    listOfResult!!.get(i).active=false
                }
                stmt?.close()

            }

        }

        var i:Int=0
        while (listOfResult!!.size>i){
            if(listOfResult!!.get(i).active!!){
                listOfResult!!.removeAt(i)
                i=0
            }
            i++
        }
        conn!!.close()
    }

    private fun showTables() {
        stmt = conn!!.createStatement()
        stmt?.executeUpdate("USE " + dataBaseName)
        stmt = conn!!.createStatement()
        val rs: ResultSet = stmt?.executeQuery("SHOW TABLES")!!

        while (rs.next()) {
            var modelSelectDataBase = modelSelect()
            modelSelectDataBase.name = rs.getString(1)
            listOfResult?.add(modelSelectDataBase)
        }
        rs.close()
        conn!!.close()
        stmt!!.close()
    }

    private fun showFields() {
        stmt = conn!!.createStatement()
        stmt?.executeUpdate("USE " + dataBaseName)
        stmt = conn!!.createStatement()
        val rs: ResultSet = stmt?.executeQuery("DESCRIBE " + tableName)!!

        while (rs.next()) {
            var modelSelectTable = modelSelectTable()
            modelSelectTable.name = rs.getString(1)
            modelSelectTable.type = rs.getString(2)
            modelSelectTable.nullity = rs.getString(3)
            modelSelectTable.key = rs.getString(4)

            listOfResultTables?.add(modelSelectTable)
        }
        rs.close()
        conn!!.close()
        stmt!!.close()
    }


    private fun createTable() {
        stmt = conn!!.createStatement()
        stmt?.executeUpdate("USE " + dataBaseName)
        stmt = conn!!.createStatement()
        var aux: String = ""
        var query: String = "CREATE TABLE " + dataBaseName + "." + tableName + " ( ";
        var afterEnter:Boolean= false
        for (parameter in listOfParameters!!) {
            if (afterEnter) {
                aux =aux+", "+parameter.name + " " + parameter.type
            }else{
                aux =aux+parameter.name + " " + parameter.type
            }
            afterEnter=true

            if (!parameter.length.equals("")) {
                aux = aux + "(" + parameter.length + ") "
            }
            if (parameter.nullable) {
                aux = aux + " NULL "
            } else {
                aux = aux + " NOT NULL "
            }

            if (parameter.aI) {
                aux = aux + "AUTO_INCREMENT "
            }

            if (!parameter.predeterminated.equals("NOTHING")) {
                aux = aux + "DEFAULT " + parameter.predeterminated
            }

        }
        var boolean: Boolean = false
        for (parameter in listOfParameters!!) {
            if (!parameter.index.equals("NOTHING")) {
                when (parameter.index) {
                    "PRIMARY" -> {

                            aux = aux + ", PRIMARY KEY(" + parameter.name + ")"

                    }

                    "UNIQUE" -> {

                                        aux = aux + ", UNIQUE(" + parameter.name + ")"


                    }
                    "INDEX"->{

                            aux = aux + ", INDEX(" + parameter.name + ")"

                    }
                    "FULLTEXT"->{

                            aux = aux + ", FULLTEXT(" + parameter.name + ")"

                    }
                    "SPATIAL"->{
                            aux = aux + ", SPATIAL(" + parameter.name + ")"

                    }

                    }
                }

            }
        aux=aux+")"
        Log.i("query ", query+aux)
        try {
            stmt!!.executeUpdate(query + aux)
        }catch (a:SQLException){
            error=true
            msgError=a.localizedMessage
        }finally {
            conn!!.close()
            stmt!!.close()
        }
        }

    private fun addingColumn() {
        stmt = conn!!.createStatement()
        stmt!!.executeUpdate("USE "+dataBaseName)
        stmt = conn!!.createStatement()
        var query:String ="ALTER TABLE "+tableName+" ADD "


        query =query+modelSingleton!!.name + " " + modelSingleton!!.type


        if (!modelSingleton!!.length.equals("")) {
            query = query + "(" + modelSingleton!!.length + ") "
        }
        if (modelSingleton!!.nullable) {
            query = query + " NULL "
        } else {
            query = query + " NOT NULL "
        }

        if (modelSingleton!!.aI) {
            query = query + "AUTO_INCREMENT "
        }

        if (!modelSingleton!!.predeterminated.equals("NOTHING")) {
            query = query + "DEFAULT " + modelSingleton!!.predeterminated
        }

        if (!modelSingleton!!.index.equals("NOTHING")) {
            when (modelSingleton!!.index) {
                "PRIMARY" -> {

                    query = query + ", PRIMARY KEY(" + modelSingleton!!.name + ")"

                }

                "UNIQUE" -> {

                    query = query + ", UNIQUE(" + modelSingleton!!.name + ")"


                }
                "INDEX" -> {

                    query = query + ", INDEX(" + modelSingleton!!.name + ")"

                }
                "FULLTEXT" -> {

                    query = query + ", FULLTEXT(" + modelSingleton!!.name + ")"

                }
                "SPATIAL" -> {
                    query = query + ", SPATIAL(" + modelSingleton!!.name + ")"

                }

            }
        }

            query = query + " AFTER " + tableAfterName
        try {
            stmt!!.executeUpdate(query)
        }catch (a:SQLException){
            error=true
            msgError=a.localizedMessage
        }finally {
            conn!!.close()
            stmt!!.close()
        }

    }

    private fun deleteTable() {
        stmt = conn!!.createStatement()
        stmt!!.executeUpdate("USE "+dataBaseName)
        for (i in 0 until listOfResult!!.size ) {

            if (listOfResult!!.get(i).active!!) {
                stmt = conn!!.createStatement()
                try {
                    stmt?.executeUpdate("DROP TABLE " + listOfResult!!.get(i).name)
                } catch (a: SQLException) {
                  error=true
                  msgError=a.localizedMessage
                }
                stmt?.close()

            }
        }
        var i:Int=0
        while (listOfResult!!.size>i){
            if(listOfResult!!.get(i).active!!){
                listOfResult!!.removeAt(i)
                i=0
            }
            i++
        }
        conn!!.close()
    }

    private fun deleteColumn() {
        stmt = conn!!.createStatement()
        stmt!!.executeUpdate("USE "+dataBaseName)
        var query:String = "ALTER TABLE " +tableName+" DROP COLUMN "
        for (i in 0 until listOfResultTables!!.size ) {

            if (listOfResultTables!!.get(i).active!!) {
                stmt = conn!!.createStatement()
                try {
                    stmt?.executeUpdate(query+listOfResultTables!!.get(i).name)
                } catch (a: SQLException) {
                    listOfResultTables!!.get(i).active=false
                    error=true
                    msgError=a.localizedMessage
                }
                stmt?.close()

            }
        }
        var i:Int=0
        while (listOfResultTables!!.size>i){
            if(listOfResultTables!!.get(i).active!!){
                listOfResultTables!!.removeAt(i)
                i=0
            }
            i++
        }
        conn!!.close()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (connected) 1 else 0)
        parcel.writeInt(operation)
        parcel.writeString(dataBaseName)
        parcel.writeString(tableName)
        parcel.writeString(collate)
        parcel.writeString(server)
        parcel.writeString(port)
        parcel.writeString(usr)
        parcel.writeString(psw)
        parcel.writeString(query)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ConnectionTask> {
        override fun createFromParcel(parcel: Parcel): ConnectionTask {
            return ConnectionTask(parcel)
        }

        override fun newArray(size: Int): Array<ConnectionTask?> {
            return arrayOfNulls(size)
        }
    }


}
