package com.example.naghost.myapplication.Activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import com.example.naghost.myapplication.Adapter.CreateTableAdapter
import com.example.naghost.myapplication.Asynctask.ConnectionTask
import com.example.naghost.myapplication.R
import com.example.naghost.myapplication.model.modelCreateTable

class CreateTable : AppCompatActivity(), View.OnClickListener {


    var adapter: CreateTableAdapter?=null;
    var listOfParameters :ArrayList<modelCreateTable>?=null
    var connectionTask:ConnectionTask?=null
    var name:String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_table)
        var floatingButton:FloatingActionButton = findViewById(R.id.floatingActionButtonCreateTable)
        floatingButton.setOnClickListener(this)
        var bundle:Bundle = getIntent().getExtras()
        var textView: TextView = findViewById(R.id.TextViewTableCreateName)
        name =bundle.getString("name")
        textView.text=name
        var number:Int=bundle.getString("columns").toInt()
        var bundleaux:Bundle = bundle.getBundle("bundle")
        connectionTask=bundleaux.getParcelable("object")
        var auxliar = resources.getStringArray(R.array.type_data)
        var type = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, auxliar)
        auxliar = resources.getStringArray(R.array.predeterminay_data)
        val predeterminary =ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, auxliar)
        auxliar = resources.getStringArray(R.array.index_data)
        val index =ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, auxliar)
        listOfParameters = ArrayList()
        for (i:Int in 0..(number-1)){
        var createView:modelCreateTable = modelCreateTable()
            listOfParameters!!.add(createView)
        }
        adapter = CreateTableAdapter(this, listOfParameters!!)
        adapter!!.type=type
        adapter!!.predetermined=predeterminary
        adapter!!.index=index


        var listViewCreateTables:ListView = findViewById(R.id.ListViewCreateTable)
        listViewCreateTables.adapter=adapter

    }

    override fun onClick(v: View?) {
        connectionTask!!.listOfParameters=listOfParameters
        connectionTask!!.operation=6
        connectionTask!!.tableName=name
        connectionTask!!.start()

        var counter:Int = 0
        while (connectionTask!!.isAlive()&& !connectionTask!!.isInterrupted()&& counter<10){
            Thread.sleep(1000)
            counter++
        }

        if (connectionTask!!.error){
            Toast.makeText(this,connectionTask!!.msgError,Toast.LENGTH_LONG).show()
            finish()
        }else{
            Toast.makeText(this,"Database are created succesfully",Toast.LENGTH_LONG).show()
            finish()
        }
    }
}
