package com.example.naghost.myapplication.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.naghost.myapplication.Asynctask.ConnectionTask
import com.example.naghost.myapplication.Holder.ViewHolder
import com.example.naghost.myapplication.Holder.ViewHolderTables
import com.example.naghost.myapplication.R
import com.example.naghost.myapplication.model.modelSelect

class SelectTableAdapter(var context: Context, items :ArrayList<modelSelect>) : BaseAdapter(){
    var items : ArrayList<modelSelect>? =null;
    var connectionTask: ConnectionTask?=null;
    var databaseName:String ="";
    init {
        this.items=items

    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var holder: ViewHolderTables?;
        var vista: View? = convertView;

        if (vista == null){
            vista = LayoutInflater.from(context).inflate(R.layout.gridviewdatabases, null)
            holder = ViewHolderTables(vista)
            vista.tag = holder
        }else{
            holder = vista.tag as? ViewHolderTables
        }

        val item = getItem(position) as modelSelect

        holder!!.nombre!!.text =item.name
        holder.item =item
        holder.checkBox!!.setOnClickListener(holder)
        holder.checkBox!!.setChecked(item.active!!)
        holder.context=context
        holder.nombre!!.setOnClickListener(holder)
        holder.connectionTask=connectionTask
        holder.databaseName=databaseName

        return vista!!
    }

    override fun getItem(position: Int): Any {
        return items?.get(position)!!
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return items?.count()!!
    }

}