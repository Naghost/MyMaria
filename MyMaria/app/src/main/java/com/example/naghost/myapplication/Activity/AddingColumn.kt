package com.example.naghost.myapplication.Activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.view.View
import android.widget.*
import com.example.naghost.myapplication.Asynctask.ConnectionTask
import com.example.naghost.myapplication.R
import com.example.naghost.myapplication.model.modelCreateTable

class AddingColumn : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        var name:EditText = findViewById(R.id.EditTextCreateTableNameAddingColumn)
        var type:Spinner = findViewById(R.id.SpinnerTypeCreateTableAddingColumn)
        var predetermined:Spinner = findViewById(R.id.SpinnerCreateTablePredeterminedAddingColumn)
        var index:Spinner = findViewById(R.id.SpinnerCreateTableIndexAddingColumn)
        var length:EditText = findViewById(R.id.EditTextCreateTableLengthAddingColumn)
        var aI:CheckBox = findViewById(R.id.CheckBoxCreateTableAIAddingColumn)
        var nullable:CheckBox = findViewById(R.id.CheckBoxCreateTableNullAddingColumn)

        var modelCreateTable:modelCreateTable = modelCreateTable()
        modelCreateTable.name=name.text.toString()
        modelCreateTable.type=type!!.selectedItem.toString()
        modelCreateTable.predeterminated=predetermined!!.selectedItem.toString()
        modelCreateTable.index=index.selectedItem.toString()
        modelCreateTable.length=length.text.toString()
        modelCreateTable.aI=aI.isChecked
        modelCreateTable.nullable=nullable.isChecked
        connectionTask!!.modelSingleton=modelCreateTable

        connectionTask!!.operation=7

        connectionTask!!.tableAfterName=nameBefore

        connectionTask!!.start()

        var counter:Int = 0
        while (connectionTask!!.isAlive()&& !connectionTask!!.isInterrupted()&& counter<10){
            Thread.sleep(1000)
            counter++
        }

        if (connectionTask!!.error){
            Toast.makeText(this,connectionTask!!.msgError, Toast.LENGTH_LONG).show()
            finish()
        }else{
            Toast.makeText(this,"Database are created succesfully", Toast.LENGTH_LONG).show()
            finish()
        }

    }
    var name:EditText? = null
    var type:Spinner? = null
    var predetermined:Spinner? = null
    var index:Spinner? = null
    var length:EditText? = null
    var aI:CheckBox? = null
    var nullable:CheckBox? = null
    var connectionTask:ConnectionTask?=null
    var nameBefore:String=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adding_column)

        name = findViewById(R.id.EditTextCreateTableNameAddingColumn)
        type = findViewById(R.id.SpinnerTypeCreateTableAddingColumn)
        predetermined = findViewById(R.id.SpinnerCreateTablePredeterminedAddingColumn)
        index = findViewById(R.id.SpinnerCreateTableIndexAddingColumn)
        length = findViewById(R.id.EditTextCreateTableLengthAddingColumn)
        aI = findViewById(R.id.CheckBoxCreateTableAIAddingColumn)
        nullable = findViewById(R.id.CheckBoxCreateTableNullAddingColumn)

        var bundle:Bundle=getIntent().getExtras();
        nameBefore=bundle.getString("before")
        var auxBundle:Bundle = bundle.getBundle("bundle")
        connectionTask=auxBundle.getParcelable("object")

        var floatingActionButton:FloatingActionButton = findViewById(R.id.floatingActionButtonAdding)
        floatingActionButton.setOnClickListener(this)

        var auxliar = resources.getStringArray(R.array.type_data)
        var type = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, auxliar)
        this.type!!.adapter=type
        auxliar = resources.getStringArray(R.array.predeterminay_data)
        val predeterminary = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, auxliar)
        this.predetermined!!.adapter=predeterminary
        auxliar = resources.getStringArray(R.array.index_data)
        val index = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, auxliar)
        this.index!!.adapter=index
    }
}
