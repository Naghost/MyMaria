package com.example.naghost.myapplication.model

class modelSelectTable(){
    var name:String = ""
    var type:String =""
    var nullity:String = ""
    var key:String=""
    var default:String = ""
    var special:String =""
    var active:Boolean? = false;

    constructor(name :String, active :Boolean, key :String,type :String,nullity :String, default:String,special :String ) : this() {
        this.name=name
        this.active=active
        this.key=key
        this.type=type
        this.nullity=nullity
        this.default=default
        this.special=special
    }
}