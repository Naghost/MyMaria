package com.example.naghost.myapplication.Activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import android.widget.Toast
import com.example.naghost.myapplication.Adapter.SelectTableAdapter
import com.example.naghost.myapplication.Asynctask.ConnectionTask
import com.example.naghost.myapplication.R
import com.example.naghost.myapplication.model.modelSelect

class DescribeDatabase : AppCompatActivity() , View.OnClickListener{
    var connectionTask: ConnectionTask?=null;
    var adapter: SelectTableAdapter?=null;
    var listOfResult:ArrayList<modelSelect>?= ArrayList();

    override fun onClick(v: View?) {
        when(v!!.getTag()){
            "create"->{
                var editTextName:EditText = findViewById(R.id.EditTextTableName)
                var editTextColumns:EditText = findViewById(R.id.EditTextTableColums)
                if (editTextColumns.text.toString().equals("0") || editTextColumns.text.toString().equals("") || editTextName.text.toString().equals("")){
                    Toast.makeText(this,"Invalid arguments",Toast.LENGTH_LONG).show()
                }else{
                    var intent:Intent = Intent(this, CreateTable::class.java)
                    intent.putExtra("name", editTextName.text.toString())
                    intent.putExtra("columns", editTextColumns.text.toString())
                    var bundle:Bundle = Bundle()
                    bundle.putParcelable("object",connectionTask)
                    intent.putExtra("bundle",bundle)
                    startActivity(intent)
                    finish()
                }
            }

            "delete" ->{
                var auxServer:String = connectionTask!!.server
                var auxPort:String = connectionTask!!.port
                var auxUsr:String = connectionTask!!.usr
                var auxPsw:String = connectionTask!!.psw
                var auxDatabase:String = connectionTask!!.dataBaseName
                connectionTask = ConnectionTask()
                connectionTask!!.server=auxServer
                connectionTask!!.port=auxPort
                connectionTask!!.usr=auxUsr
                connectionTask!!.psw=auxPsw
                connectionTask!!.dataBaseName=auxDatabase
                connectionTask!!.operation=8
                connectionTask!!.listOfResult=listOfResult
                connectionTask!!.start()
                while(connectionTask!!.isAlive && !connectionTask!!.isInterrupted){
                    Thread.sleep(1000)
                }
                if (connectionTask!!.error){
                    Toast.makeText(this,connectionTask!!.msgError, Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this,"Table delete succesfully", Toast.LENGTH_LONG).show()
                }
                adapter!!.notifyDataSetChanged()

            }
            "search" ->{
                var intent: Intent = Intent(this, CreateSqlQuery::class.java)
                var bundle:Bundle = Bundle()
                bundle.putParcelable("object", connectionTask)
                intent.putExtra("bundle",bundle)
                startActivity(intent)
            }
        }


    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_describe_database)

        var bundle:Bundle = getIntent().getExtras();
        var bundleAux:Bundle =bundle.getBundle("holi")
        connectionTask =bundleAux.getParcelable("Object") as ConnectionTask
        var auxName:String =bundleAux.getString("name");
        connectionTask!!.listOfResult=listOfResult
        connectionTask!!.operation=4
        connectionTask!!.dataBaseName=auxName
        connectionTask!!.start()
        var counter:Int = 0
        while (connectionTask!!.isAlive()&& !connectionTask!!.isInterrupted()&& counter<10){
            Thread.sleep(1000)
            counter++
        }
        var listView: ListView = findViewById(R.id.listViewTables)
        var button: Button =findViewById(R.id.buttonCreateTable)
        button.setOnClickListener(this)
        adapter =  SelectTableAdapter(this, listOfResult!!)
        adapter!!.connectionTask = connectionTask;
        adapter!!.databaseName=auxName
        listView.adapter=adapter
        var floatingActionButton:FloatingActionButton = findViewById(R.id.deleteTableButtonAction3)
        floatingActionButton.setOnClickListener(this)
        var floatingActionButtonSearch:FloatingActionButton = findViewById(R.id.floatingActionButton2Search)
        floatingActionButtonSearch.setOnClickListener(this)
    }
}
