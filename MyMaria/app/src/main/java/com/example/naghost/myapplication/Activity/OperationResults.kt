package com.example.naghost.myapplication.Activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import android.widget.Toast
import com.example.naghost.myapplication.Asynctask.ConnectionTask
import com.example.naghost.myapplication.R
import java.sql.ResultSet

class OperationResults : AppCompatActivity() {

    var connectionTask:ConnectionTask?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_operation_results)

        var bundle:Bundle = getIntent().getExtras()
        var auxBundle:Bundle = bundle.getBundle("bundle")
        connectionTask= auxBundle.getParcelable("object")

        connectionTask!!.operation=11
        connectionTask!!.start()

        while(connectionTask!!.isAlive && !connectionTask!!.isInterrupted){
            Thread.sleep(1000)
        }
        if (connectionTask!!.error){
            Toast.makeText(this,connectionTask!!.msgError, Toast.LENGTH_LONG).show()
            finish()
        }else{
            Toast.makeText(this,"Operation are made succesfully", Toast.LENGTH_LONG).show()
            var rs: ResultSet = connectionTask!!.rs!!

            var tableLayout:TableLayout = findViewById(R.id.tableLayout)
            var tableRow:TableRow?=null
            var textView:TextView?=null
            tableRow = TableRow(this)
            for (i in 1..rs.metaData.columnCount){

                textView = TextView(this)
                textView.setPadding(10,10,10,10)
                textView.setText(rs.metaData.getColumnLabel(i))
                tableRow.addView(textView)
            }
            tableLayout.addView(tableRow)
            while (rs.next()) {
                tableRow = TableRow(this)
                for (i in 1..rs.metaData.columnCount){

                    textView = TextView(this)
                    textView.setPadding(10,10,10,10)
                    textView.setText(rs.getString(i))
                    tableRow.addView(textView)
                }
                tableLayout.addView(tableRow)
            }
        }



    }
}
