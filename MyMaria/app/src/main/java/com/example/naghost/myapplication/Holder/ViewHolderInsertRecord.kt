package com.example.naghost.myapplication.Holder

import android.app.Activity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.example.naghost.myapplication.R
import com.example.naghost.myapplication.model.modelInsertRecord
import com.example.naghost.myapplication.model.modelSelect


class ViewHolderInsertRecord(vista: View): Activity(), TextWatcher {
    override fun afterTextChanged(s: Editable?) {
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            item!!.value=s!!.toString()

    }

    var nombre: TextView? =null
    var editText: EditText? =null
    var item: modelInsertRecord? =null
    init {
        nombre = vista.findViewById(R.id.textViewFieldName)
        editText = vista.findViewById(R.id.editTextValue)
        editText!!.addTextChangedListener(this)
    }

}
