package com.example.naghost.myapplication.Activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.util.Log
import android.view.View
import android.widget.ListView
import android.widget.Toast
import com.example.naghost.myapplication.Adapter.SelectDataBaseAdapter
import com.example.naghost.myapplication.Asynctask.ConnectionTask
import com.example.naghost.myapplication.R
import com.example.naghost.myapplication.model.modelSelect
import android.content.DialogInterface
import android.os.Build
import android.support.v7.app.AlertDialog


class SelectDataBaseActivity : AppCompatActivity(), View.OnClickListener {

    var listOfResult:ArrayList<modelSelect>?=null;
    var connectionTask:ConnectionTask? = null;
    var adaptador:SelectDataBaseAdapter? = null;
    override fun onClick(v: View?) {
       when(v!!.getTag()){
           "create"-> {
               var parameterObject:Bundle = Bundle()
               parameterObject.putParcelable("Object",connectionTask)
               var intent:Intent = Intent(this,CreateDatabase::class.java)
                intent.putExtra("holi",parameterObject)
               finish()
               startActivity(intent);



           }
           "delete"->{

               val builder: AlertDialog.Builder
               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                   builder = AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert)
               } else {
                   builder = AlertDialog.Builder(this)
               }
               builder.setTitle("Delete databases")
                       .setMessage("Are you sure you want to delete this databases?")
                       .setPositiveButton(android.R.string.yes, DialogInterface.OnClickListener { dialog, which ->
                           var auxServer:String = connectionTask!!.server
                           var auxPort:String = connectionTask!!.port
                           var auxUsr:String = connectionTask!!.usr
                           var auxPsw:String = connectionTask!!.psw
                           connectionTask = ConnectionTask()
                           connectionTask!!.server=auxServer
                           connectionTask!!.port=auxPort
                           connectionTask!!.usr=auxUsr
                           connectionTask!!.psw=auxPsw
                           connectionTask!!.operation=3
                           connectionTask!!.listOfResult=listOfResult
                           connectionTask!!.start()
                           while(connectionTask!!.isAlive && !connectionTask!!.isInterrupted){
                               Thread.sleep(1000)
                           }
                           if (connectionTask!!.error){
                               Toast.makeText(this,connectionTask!!.msgError, Toast.LENGTH_LONG).show()
                           }
                           adaptador!!.notifyDataSetChanged()

                       })
                       .setNegativeButton(android.R.string.no, DialogInterface.OnClickListener { dialog, which ->
                           // do nothing
                       })
                       .setIcon(android.R.drawable.ic_dialog_alert)
                       .show()
           }

       }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_data_base)
        var listViewDatabases:ListView = findViewById(R.id.ListViewDataBases) as ListView;
        var floatingActionCreate: FloatingActionButton = findViewById(R.id.createDatabaseButtonAction) as FloatingActionButton;
        var floatingActionDelete: FloatingActionButton = findViewById(R.id.deleteDatabaseButtonAction) as FloatingActionButton;
        floatingActionCreate.setOnClickListener(this)
        floatingActionDelete.setOnClickListener(this)
        listOfResult = ArrayList();
        var list:ArrayList<String> = ArrayList();
        if(!intent.getStringExtra("server").isNullOrBlank()) {
            list.add(intent.getStringExtra("server"))
            list.add(intent.getStringExtra("port"))
            list.add(intent.getStringExtra("user"))
            list.add(intent.getStringExtra("password"))
            connectionTask = ConnectionTask()
            connectionTask!!.listParameterConnection=list;
            connectionTask!!.arrayListToParameters()
        }else{
            var bundle:Bundle = getIntent().getExtras();
            var bundleAux:Bundle =bundle.getBundle("holi")
            connectionTask =bundleAux.getParcelable("Object") as ConnectionTask

        }
        connectionTask!!.listOfResult = listOfResult
        connectionTask!!.operation=1
        connectionTask!!.start()
        var counter:Int=0;
        while (connectionTask!!.isAlive && !connectionTask!!.isInterrupted && counter<=10){
            Log.i("Estado", connectionTask!!.state.toString())
            Thread.sleep(1000)
            counter++
        }
        adaptador = SelectDataBaseAdapter(this, listOfResult!!)
        adaptador!!.connectionTask = connectionTask;
        listViewDatabases.adapter=adaptador



        if (!connectionTask!!.connected){

            val changeActivity = Intent(this, MainActivity::class.java)
            Toast.makeText(this, "Connection time out, check your server",Toast.LENGTH_LONG).show()
            connectionTask!!.interrupt()
            finish()
            startActivity(changeActivity)


        }

    }


}
