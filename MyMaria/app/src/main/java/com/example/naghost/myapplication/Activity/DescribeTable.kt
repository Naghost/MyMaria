package com.example.naghost.myapplication.Activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.view.View
import android.widget.*
import com.example.naghost.myapplication.Adapter.SelectFieldsTableAdapter
import com.example.naghost.myapplication.Asynctask.ConnectionTask
import com.example.naghost.myapplication.R
import com.example.naghost.myapplication.model.modelSelectTable

class DescribeTable : AppCompatActivity(), View.OnClickListener{
    var connectionTask: ConnectionTask?=null;
    var listOfResultTables:ArrayList<modelSelectTable>?= ArrayList();
    var listOfSpinner:ArrayList<String>?= ArrayList()
    var adapter: SelectFieldsTableAdapter?=null;
    var spinner:Spinner?=null

    override fun onClick(v: View?) {

        when(v!!.tag){
            "create" ->{
                var intent: Intent = Intent(this, AddingColumn::class.java)
                intent.putExtra("before",spinner!!.selectedItem.toString() )
                var bundle:Bundle = Bundle()
                bundle.putParcelable("object", connectionTask)
                intent.putExtra("bundle",bundle)
                startActivity(intent)
                finish()
            }

            "delete"->{
                var auxServer:String = connectionTask!!.server
                var auxPort:String = connectionTask!!.port
                var auxUsr:String = connectionTask!!.usr
                var auxPsw:String = connectionTask!!.psw
                var auxDatabase:String = connectionTask!!.dataBaseName
                var auxTable:String = connectionTask!!.tableName
                connectionTask = ConnectionTask()
                connectionTask!!.server=auxServer
                connectionTask!!.port=auxPort
                connectionTask!!.usr=auxUsr
                connectionTask!!.psw=auxPsw
                connectionTask!!.dataBaseName=auxDatabase
                connectionTask!!.operation=9
                connectionTask!!.tableName=auxTable
                connectionTask!!.listOfResultTables=listOfResultTables
                connectionTask!!.start()

                while(connectionTask!!.isAlive && !connectionTask!!.isInterrupted){
                    Thread.sleep(1000)
                }
                if (connectionTask!!.error){
                    Toast.makeText(this,connectionTask!!.msgError, Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this,"Table delete succesfully", Toast.LENGTH_LONG).show()
                }
                adapter!!.notifyDataSetChanged()
            }

            "search" ->{
                var intent: Intent = Intent(this, CreateSqlQuery::class.java)
                var bundle:Bundle = Bundle()
                bundle.putParcelable("object", connectionTask)
                intent.putExtra("bundle",bundle)
                startActivity(intent)
                finish()
            }

            "add" ->{
                var intent: Intent = Intent(this, RecordInsert::class.java)
                var bundle:Bundle = Bundle()
                bundle.putParcelable("object", connectionTask)
                intent.putExtra("bundle",bundle)
                startActivity(intent)
                finish()
            }


        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_describe_table)
        var bundle:Bundle = getIntent().getExtras();
        var bundleAux:Bundle =bundle.getBundle("holi")

        connectionTask =bundleAux.getParcelable("Object") as ConnectionTask
        connectionTask!!.operation=5
        connectionTask!!.listOfResultTables=listOfResultTables
        connectionTask!!.start()
        var counter:Int = 0
        while (connectionTask!!.isAlive()&& !connectionTask!!.isInterrupted()&& counter<10){
            Thread.sleep(1000)
            counter++
        }
        var listView:ListView = findViewById(R.id.listViewTables) as ListView
        spinner = findViewById(R.id.spinnerTables)
        for (result in listOfResultTables!!){
            listOfSpinner!!.add(result.name)
        }

        spinner!!.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item,listOfSpinner )
        adapter =  SelectFieldsTableAdapter(this, listOfResultTables!!)
        adapter!!.connectionTask = connectionTask;

        listView.adapter=adapter
        var button: Button=findViewById(R.id.buttonAddingColumn)
        button.setOnClickListener(this)
        var floatingActionButton:FloatingActionButton = findViewById(R.id.deleteColumnButtonAction2)
        floatingActionButton.setOnClickListener(this)
        var floatingActionButtonSearch:FloatingActionButton = findViewById(R.id.floatingActionButton2Search)
        floatingActionButtonSearch.setOnClickListener(this)
        var floatingActionButtonAdd:FloatingActionButton = findViewById(R.id.AddValue)
        floatingActionButtonAdd.setOnClickListener(this)
    }

}
