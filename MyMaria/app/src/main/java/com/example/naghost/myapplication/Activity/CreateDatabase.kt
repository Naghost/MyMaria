package com.example.naghost.myapplication.Activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.example.naghost.myapplication.R
import com.example.naghost.myapplication.Asynctask.ConnectionTask


class CreateDatabase : AppCompatActivity(), View.OnClickListener {
    var spinner:Spinner? = null;
    var editText:EditText?=null;
    var connectionTask:ConnectionTask?=null;
    override fun onClick(v: View?) {
        connectionTask!!.dataBaseName=editText!!.text.toString()
        connectionTask!!.collate=spinner!!.selectedItem.toString()
        connectionTask!!.operation=2
        connectionTask!!.start()

        while (connectionTask!!.isAlive && !connectionTask!!.isInterrupted && !connectionTask!!.error){
            Thread.sleep(1000)
        }
        if (!connectionTask!!.error) {
            Toast.makeText(this, "Database are created successfully", Toast.LENGTH_LONG).show()
        }else{
            Toast.makeText(this, connectionTask!!.msgError, Toast.LENGTH_LONG).show()
        }

        var parameterObject:Bundle = Bundle()
        parameterObject.putParcelable("Object",connectionTask)
        var intent: Intent = Intent(this,SelectDataBaseActivity::class.java)
        intent.putExtra("holi",parameterObject)
        startActivity(intent);
        finish()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_database)
        var bundle:Bundle = getIntent().getExtras();
        var bundleAux:Bundle =bundle.getBundle("holi")
        connectionTask =bundleAux.getParcelable("Object") as ConnectionTask
        spinner =  findViewById(R.id.spinnerCreateDatabaseSpinner) as Spinner;
        editText = findViewById(R.id.EditTextCreateDataBaseEdit) as EditText;
        val cortejamiento = resources.getStringArray(R.array.cotejamiento)

        val adapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, cortejamiento)

        spinner!!.setAdapter(adapter)


        var button:Button = findViewById(R.id.buttonCreateDatabase) as Button
        button.setOnClickListener(this)
    }
}
