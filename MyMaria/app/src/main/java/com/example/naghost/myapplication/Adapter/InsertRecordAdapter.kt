package com.example.naghost.myapplication.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.naghost.myapplication.Asynctask.ConnectionTask
import com.example.naghost.myapplication.Holder.ViewHolderInsertRecord
import com.example.naghost.myapplication.R
import com.example.naghost.myapplication.model.modelInsertRecord


class InsertRecordAdapter(var context: Context, items :ArrayList<modelInsertRecord>) : BaseAdapter(){
    var items : ArrayList<modelInsertRecord>? =null;
    var connectionTask: ConnectionTask?=null;
    init {
        this.items=items

    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var holder: ViewHolderInsertRecord? =null;
        var vista: View? = convertView;

        if (vista == null){
            vista = LayoutInflater.from(context).inflate(R.layout.listviewinsertrecord, null)
            holder = ViewHolderInsertRecord(vista)
            vista.tag = holder
        }else{
            holder = vista.tag as? ViewHolderInsertRecord
        }

        val item = getItem(position) as modelInsertRecord

        holder?.item =item
        holder?.nombre?.text =item.name
        holder?.editText?.setText(item.value)

        return vista!!
    }

    override fun getItem(position: Int): Any {
        return items?.get(position)!!
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return items?.count()!!
    }

}