package com.example.naghost.myapplication.Activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.view.View
import android.widget.Button
import android.widget.ListView
import com.example.naghost.myapplication.Adapter.InsertRecordAdapter
import com.example.naghost.myapplication.Asynctask.ConnectionTask
import com.example.naghost.myapplication.R
import com.example.naghost.myapplication.model.modelInsertRecord
import com.example.naghost.myapplication.model.modelSelectTable

class RecordInsert : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        var auxServer:String = connectionTask!!.server
        var auxPort:String = connectionTask!!.port
        var auxUsr:String = connectionTask!!.usr
        var auxPsw:String = connectionTask!!.psw
        var auxDatabase:String = connectionTask!!.dataBaseName
        var auxTable:String = connectionTask!!.tableName
        connectionTask = ConnectionTask()
        connectionTask!!.server=auxServer
        connectionTask!!.port=auxPort
        connectionTask!!.usr=auxUsr
        connectionTask!!.psw=auxPsw
        connectionTask!!.dataBaseName=auxDatabase
        connectionTask!!.tableName=auxTable
        connectionTask!!.operation=12
        connectionTask!!.listOfResultsConverted=listOfResultsConverted
        connectionTask!!.start()

        var counter:Int = 0
        while (connectionTask!!.isAlive()&& !connectionTask!!.isInterrupted()&& counter<10){
            Thread.sleep(1000)
            counter++
        }

    }

    var connectionTask: ConnectionTask?=null;
    var listOfResultTables:ArrayList<modelSelectTable>?= ArrayList();
    var listOfResultsConverted:ArrayList<modelInsertRecord>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record_insert)
        var bundle:Bundle = getIntent().getExtras();

        var bundleAux:Bundle =bundle.getBundle("bundle")
        connectionTask =bundleAux.getParcelable("object") as ConnectionTask

        connectionTask!!.operation=5
        connectionTask!!.listOfResultTables=listOfResultTables
        connectionTask!!.start()

        var counter:Int = 0
        while (connectionTask!!.isAlive()&& !connectionTask!!.isInterrupted()&& counter<10){
            Thread.sleep(1000)
            counter++
        }

        var modelInsertRecord:modelInsertRecord?=null
        for (record in this!!.listOfResultTables!!){
            modelInsertRecord= modelInsertRecord()
            modelInsertRecord.name=record.name
            listOfResultsConverted!!.add(modelInsertRecord)
        }

        var listView:ListView = findViewById(R.id.listViewInsert)
        var adapter:InsertRecordAdapter = InsertRecordAdapter(this, listOfResultsConverted!!)
        listView.adapter=adapter

        var button: FloatingActionButton = findViewById(R.id.buttonExecuteInsert)
        button.setOnClickListener(this)

    }
}
