package com.example.naghost.myapplication.Holder



import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.widget.CheckBox
import android.widget.TextView
import com.example.naghost.myapplication.Activity.DescribeDatabase
import com.example.naghost.myapplication.Activity.DescribeTable
import com.example.naghost.myapplication.Asynctask.ConnectionTask
import com.example.naghost.myapplication.R
import com.example.naghost.myapplication.model.modelSelect

class ViewHolderTables(vista: View): OnClickListener, Activity() {
    var context:Context?=null;
    var connectionTask:ConnectionTask?=null;
    var nombre: TextView? =null
    var checkBox: CheckBox? =null
    var databaseName:String = "";
    var item:modelSelect? =null
    init {
        nombre = vista.findViewById(R.id.textViewDatabase)
        checkBox = vista.findViewById(R.id.checkBoxDatabase)
    }
    override fun onClick(v: View?) {
        when(v!!.tag){
            "checkbox" ->{ item!!.active = item!!.active != true}
            "textview" ->{
                var parameterObject:Bundle = Bundle()
                connectionTask!!.dataBaseName=databaseName
                connectionTask!!.tableName=nombre!!.text.toString()
                parameterObject.putParcelable("Object",connectionTask)
                var intent:Intent = Intent(context,DescribeTable::class.java)
                intent.putExtra("holi",parameterObject)
                context!!.startActivity(intent);

            }

        }

    }



}
