package com.example.naghost.myapplication.Holder

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import com.example.naghost.myapplication.Activity.DescribeTable
import com.example.naghost.myapplication.Asynctask.ConnectionTask
import com.example.naghost.myapplication.R
import com.example.naghost.myapplication.model.modelSelect
import com.example.naghost.myapplication.model.modelSelectTable
import org.w3c.dom.Text

class ViewHolderTablesFields(vista: View): View.OnClickListener, Activity() {
    var context: Context?=null;
    var connectionTask: ConnectionTask?=null;
    var key: TextView?=null
    var nulleable: TextView?=null
    var type:TextView?=null
    var nombre: TextView? =null
    var checkBox: CheckBox? =null
    var databaseName:String = "";
    var item: modelSelectTable? =null
    init {
        nombre = vista.findViewById(R.id.textViewField)
        checkBox = vista.findViewById(R.id.checkBoxDatabase)
        key = vista.findViewById(R.id.textViewKey)
        nulleable = vista.findViewById(R.id.textViewNull)
        type = vista.findViewById(R.id.textViewType)
    }
    override fun onClick(v: View?) {
        when(v!!.tag){
            "checkbox" ->{ item!!.active = item!!.active != true}

        }

    }



}
